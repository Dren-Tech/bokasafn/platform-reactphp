<?php
declare(strict_types=1);

/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * index.php of project bokasafn.
 * Created by user marian at 2019-01-05.
 */

/** @var \Bokasafn\App $app */
$app = require_once dirname(__DIR__) . "/src/bootstrap.php";

$app->init();
$app->run();