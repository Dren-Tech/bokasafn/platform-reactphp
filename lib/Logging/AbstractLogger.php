<?php
declare(strict_types=1);

/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * AbstractLogger.php of project bokasafn.
 * Created by user marian at 2019-01-06.
 */

namespace DrenTech\Logging;


use Psr\Log\LoggerInterface;

abstract class AbstractLogger implements LoggerInterface
{

}