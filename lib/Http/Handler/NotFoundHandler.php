<?php
declare(strict_types=1);

/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * NotFoundHandler.php of project bokasafn.
 * Created by user marian at 2019-01-13.
 */

namespace DrenTech\Http\Handler;


use DrenTech\Http\HandlerInterface;
use DrenTech\Http\HttpMethod;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use React\Http\Response;

class NotFoundHandler implements HandlerInterface
{

    public function route(): string
    {
        return "/404";
    }

    public function method(): string
    {
        return HttpMethod::GET;
    }

    public function description(): string
    {
        return "Handles request where no handler could be found - 404";
    }

    public function declareInput(): array
    {
        // TODO: Implement declareInput() method.
    }

    public function handle(RequestInterface $request): ResponseInterface
    {
        return new Response(
            404,
            [],
            "404 - Page not found"
        );
    }

    public function cacheTtlSeconds(): int
    {
        // TODO: Implement cacheTtlSeconds() method.
    }
}