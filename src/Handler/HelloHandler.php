<?php
declare(strict_types=1);

/**
 * Copyright (c) 2019 Marian Sievers
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * HelloHandler.php of project bokasafn.
 * Created by user marian at 2019-01-05.
 */

namespace Bokasafn\Handler;


use Bokasafn\Service\InfluxMetricService;
use DrenTech\Http\HandlerInterface;
use DrenTech\Http\HttpMethod;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use React\Http\Response;

class HelloHandler implements HandlerInterface
{
    /**
     * @var InfluxMetricService
     */
    private $metricService;

    public function __construct(InfluxMetricService $metricService)
    {
        $this->metricService = $metricService;
    }

    public function route(): string
    {
        return "/hello";
    }

    public function description(): string
    {
        return "A hello world handler to demonstrate the code";
    }

    public function handle(RequestInterface $request): ResponseInterface
    {
        $this->metricService->writeMetric('request_hello', 1);

        return new Response(
            200,
            ['Content-Type' => 'text/plain'],
            "Hello World!"
        );
    }

    public function cacheTtlSeconds(): int
    {
        return 60*60*24;
    }

    public function declareInput(): array
    {
        // TODO: Implement declareInput() method.
    }

    public function method(): string
    {
        return HttpMethod::GET;
    }
}